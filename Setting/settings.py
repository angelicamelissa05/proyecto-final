# Importando libraries
from flask import Flask, request, Response, jsonify
from flask_sqlalchemy import SQLAlchemy


#  Creando una instancia de  flask app
app = Flask(__name__)

# Configuracion de base de datos database
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///database.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False