from videos import *


# Ruta para obtener todos los videos 
@app.route('/videos', methods=['GET'])
def get_video():
    '''Funcion para obtener todas lo videos en la base de datos'''
    return jsonify({'Videos': Video.get_all_video()})


# Ruta para obtener el video por id
@app.route('/videos/<int:id>', methods=['GET'])
def get_video_by_id(id):
    return_value = Video.get_video(id)
    return jsonify(return_value)


# Ruta para agragar un nuevo video
@app.route('/videos', methods=['POST'])
def add_video():
    '''Funcion para agregar nuevo video a nuestra base de datos'''
    request_data = request.get_json()  # obteniendo datos del cliente
    Video.add_video(request_data["title"], request_data["year"],
                    request_data["genre"])
    response = Response("Video agregado", 201, mimetype='application/json')
    return response


# Ruta para actualizar el video con el metodo PUT
@app.route('/videos/<int:id>', methods=['PUT'])
def update_video(id):
    '''Funcion para editar videos en nuestra base de datos usando ID del video'''
    request_data = request.get_json()
    Video.update_video(id, request_data['title'], request_data['year'], request_data['genre'])
    response = Response("Video actualizado", status=200, mimetype='application/json')
    return response


# Ruta para eliminar el video usando el metodo DELETE
@app.route('/videos/<int:id>', methods=['DELETE'])
def remove_video(id):
    '''Funcion para eliminar videos de nuestra base de datos'''
    Video.delete_video(id)
    response = Response("Video eliminado", status=200, mimetype='application/json')
    return response


if __name__ == "__main__":
    app.run(port=1234, debug=True)
