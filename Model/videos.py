from settings import *
import json

# Inicializando base de datos
db = SQLAlchemy(app)


# La clase Video heredara el db.Model de SQLAlchemy
class Video(db.Model):
    __tablename__ = 'videos'  # creando un nombre de tabla
    id = db.Column(db.Integer, primary_key=True)  # esta es la clave principal
    title = db.Column(db.String(80), nullable=False)
    # nullable es falso, por lo que la columna no puede estar vacía
    year = db.Column(db.Integer, nullable=False)
    genre = db.Column(db.String(80), nullable=False)

    def json(self):
        return {'id': self.id, 'title': self.title,
                'year': self.year, 'genre': self.genre}
        # este metodo que estamos definiendo convertira nuestra salida a json

    def add_video(_title, _year, _genre):
        '''Funcion para agregar videos a la base de datos usando _title, _year, _genre
        como parametros'''
        # creando una instancia de nuestro constructor de videos
        new_video = Video(title=_title, year=_year, genre=_genre)
        db.session.add(new_video)  # aagregar nuevo video a la sesión de la base de datos
        db.session.commit()  # confirmar cambios en la sesion

    def get_all_video():
        '''Funcion para obtener todas los videos en nuestra base de datos'''
        return [Video.json(video) for video in Video.query.all()]

    def get_video(_id):
        '''Funcion para obtener el video usando el id del video como parametro'''
        return [Video.json(Video.query.filter_by(id=_id).first())]
        # Video.json() convierte nuestra salida a json
        # filter_by method filtra la consulta por el id
        # .first() muestra el primer valor

    def update_video(_id, _title, _year, _genre):
        '''Funcion para actualizar los detalles de un video usando id, titulo,
        anio and genero como parametros'''
        video_to_update = Video.query.filter_by(id=_id).first()
        video_to_update.title = _title
        video_to_update.year = _year
        video_to_update.genre = _genre
        db.session.commit()

    def delete_video(_id):
        '''Funcion para eliminar un video de nuestra base de datos usando
           el id del video como parametro'''
        Video.query.filter_by(id=_id).delete()
        # filtrar por id y eliminar
        db.session.commit()  # confirmando el nuevo cambio en nuestra base de datos
